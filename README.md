# Reltio Util Export #

# Description #
This utility can help in export the data from Reltio and download the files into the local machine

##Change Log
```
 
  Version           : 1.3.4
  Last Updated Date : 29th June 2020
  Last Updated By   : Shivaputrappa Patil
  Last Changes      : core-cst version changed to 1.4.9
                    : Upgraded to use export version V2 and 


  Version           : 1.3.1
  Last Updated Date : 19th Sept 2019
  Last Updated By   : Vignesh Chandran
  Last Changes      : core-cst version changed to 1.4.9
                    : Client Credential Implemenation (https://reltio.jira.com/browse/ROCS-62)
                    : Bug fix for returning the full file name in v2 Export (https://reltio.jira.com/browse/ROCS-75)



  Version           : 1.3.0
  Last Updated Date : 2nd July 2019
  Last Updated By   : Vignesh
  Last Changes      : Added support for v2 version of export.(Smart Export - https://reltio.jira.com/browse/ROCS-32)
                    : Added Proxy Support (https://reltio.jira.com/browse/ROCS-49)
                    : Standardization of Jar (https://reltio.jira.com/browse/ROCS-33)
                    : Added Password Encryption (https://reltio.jira.com/browse/ROCS-60) 
                    : Log4j2 added
  
  
  Version           : 1.2.6
  Last Updated Date : 
  Last Updated By   : Shiva
  Last Changes      : Inital Load

```
##Contributing 
Please visit our [Contributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/a8e997d2547bf4df9f69bf3e7f2fcefe28d7e551/CodeOfConduct.md?at=master&fileviewer=file-view-default) to learn more about our contribution guidlines

## Licensing
```
Copyright (c) 2017 Reltio


Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
```

## Quick Start 
To learn about dependencies, building and executing the tool view our [quick start](https://bitbucket.org/reltio-ondemand/util-export/src/master/QuickStart.md).

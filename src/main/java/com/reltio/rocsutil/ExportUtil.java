package com.reltio.rocsutil;

import com.google.gson.Gson;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.rocs.export.dto.TaskResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.List;

public abstract class ExportUtil {

    private static final Logger LOG = LogManager.getLogger(ExportUtil.class);

    /**
     * This method will download from url to a file
     *
     * @throws Exception
     */
    public static File downloadFile(String decryptedUrl, String file) throws Exception {

        LOG.debug("Downloading file starts from " + decryptedUrl + ", to = " + file);

        try (BufferedInputStream in = new BufferedInputStream(new URL(decryptedUrl).openStream());
             FileOutputStream fileOutputStream = new FileOutputStream(file)) {

            byte[] dataBuffer = new byte[1024];
            int bytesRead;

            while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                fileOutputStream.write(dataBuffer, 0, bytesRead);
            }

            LOG.debug("File Download successfully to = " + file);
        } catch (Exception e) {
            LOG.error("Failed to download file = " + decryptedUrl, e);
            throw e;
        }

        return new File(file);
    }

    public static File downloadFile(String decryptedUrl, String file, String path) throws Exception {

        LOG.debug("Downloading file starts from " + decryptedUrl + ", to = " + file);

        File directory = new File(path);
        if (!directory.exists()) {
            directory.mkdir();
            // If you require it to make the entire directory path including parents,
            // use directory.mkdirs(); here instead.
        }

        File file1 = new File(path + "/" + file);
        try (BufferedInputStream in = new BufferedInputStream(new URL(decryptedUrl).openStream());
             FileOutputStream fileOutputStream = new FileOutputStream(file1.getAbsolutePath())) {

            byte[] dataBuffer = new byte[1024];
            int bytesRead;

            while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                fileOutputStream.write(dataBuffer, 0, bytesRead);
            }

            LOG.debug("File Download successfully to = " + file);
        } catch (Exception e) {
            LOG.error("Failed to download file = " + decryptedUrl, e);
            throw e;
        }

        return new File(file1.getAbsolutePath());
    }

    /*
     *
     * This method will decrypt url from reltio api
     */

    public static String decryptExportUrl(String decryptApiUrl, ReltioAPIService service, String exportUrl)
            throws Exception {

        String decryptedUrl;

        try {

            decryptedUrl = service.post(decryptApiUrl, exportUrl);
        } catch (ReltioAPICallFailureException re) {
            LOG.error("Error decrypting url = " + exportUrl + re.getErrorResponse());
            throw re;
        }

        LOG.debug("decrypted url = " + decryptedUrl);

        return decryptedUrl;
    }

    /**
     * /** This will get latest task details
     */
    public static TaskResponse getTaskDetails(List<String> taskIds, String taskUrl, ReltioAPIService service)
            throws Exception {

        TaskResponse response = null;

        for (String taskId : taskIds) {
            String taskResponse = service.get(taskUrl + taskId);
            response = new Gson().fromJson(taskResponse, TaskResponse.class);

            break;
        }

        return response;
    }
}

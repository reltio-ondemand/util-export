/**
 *
 */
package com.reltio.rocs.main;

import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.util.Util;
import com.reltio.rocs.reltioexporter.builder.ExportServiceBuilder;
import com.reltio.rocs.service.Export;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * @author spatil
 *
 */
public class Exporter {

    private static final Logger LOG = LogManager.getLogger(Exporter.class);

    private List<String> requiredKeys = Arrays.asList("ENVIRONMENT_URL", "TYPE_OF_DATA", "TENANT_ID", "USERNAME",
            "PASSWORD", "AUTH_URL", "FILTER");

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {

        if (args.length == 0) {
            LOG.error("Config file path required!");
            System.exit(0);
        }

        List<String> requiredKeys = Arrays.asList(
                "ENVIRONMENT_URL",
                "TYPE_OF_DATA",
                "TENANT_ID",
                "AUTH_URL",
                "FILTER"
        );


        Properties config = Util.getProperties(args[0], "PASSWORD", "CLIENT_CREDENTIALS");
        ReltioAPIService service = Util.getReltioService(config);

        List<String> usernamePasswordPropertyList = new ArrayList<>(Arrays.asList("USERNAME", "PASSWORD"));
        List<String> clientCredentialPropertyList = new ArrayList<>(Collections.singletonList("CLIENT_CREDENTIALS"));


        Map<List<String>, List<String>> mutualExclusiveProps = new HashMap<>();
        mutualExclusiveProps.put(usernamePasswordPropertyList, clientCredentialPropertyList);

        List<String> missingKeys = Util.listMissingProperties(config, requiredKeys, mutualExclusiveProps);
        if (!Util.isEmpty(missingKeys)) {
            LOG.error("Properties are missing \n" + String.join("\n", missingKeys));
            System.exit(0);
        }

        //Proxy setup
        if (!Util.isEmpty(config.getProperty("HTTP_PROXY_HOST")) &&
                !Util.isEmpty(config.getProperty("HTTP_PROXY_PORT"))) {
            Util.setHttpProxy(config);
        }

        String version = config.getProperty("VERSION", "v2");
        if (version.equalsIgnoreCase("v2")) {

            LOG.info("Export will utilize v2 version of export");

        } else if (version.equalsIgnoreCase("v1")) {
            LOG.info("Export will utilize v1 version of export");

        } else {
            LOG.error("Invalid export version type");
        }

        Export export = ExportServiceBuilder.build(config, service);
        List<String> file = export.exportData(version);


        LOG.debug("Export data is copied to file = " + file);

    }

    /**
     * This will export data to zip file and return buffered reader
     **/
    public List<BufferedReader> getBufferredReaderExport(Properties config) throws Exception {

        List<String> missingKeys = Util.listMissingProperties(config, requiredKeys);

        if (!Util.isEmpty(missingKeys)) {
            LOG.error("Properties are missing \n" + String.join("\n", missingKeys));
            System.exit(0);
        }

        String decryptedUrl = "";// export(config);
        File file = com.reltio.rocsutil.ExportUtil.downloadFile(decryptedUrl,
                config.getProperty("EXPORT_FILE") == null ? System.currentTimeMillis() + ".zip"
                        : config.getProperty("EXPORT_FILE"));

        List<BufferedReader> reader = readZipFile(file.getAbsolutePath());

        return reader;
    }

    /**
     * This will export data to a zip file and return file path
     */
    public List<String> exportData(Properties config) throws Exception {

        List<String> missingKeys = Util.listMissingProperties(config, requiredKeys);
        if (!Util.isEmpty(missingKeys)) {
            LOG.error("Properties are missing \n" + String.join("\n", missingKeys));
            System.exit(0);
        }
        ReltioAPIService service = Util.getReltioService(config);
        Export export = ExportServiceBuilder.build(config, service);
        return export.exportData(config.getProperty("VERSION"));
    }

    /**
     * This will give buffered reader for zip file content
     */
    public List<BufferedReader> readZipFile(String file) throws Exception {
        ZipFile zip = new ZipFile(file);
        List<BufferedReader> reader = new ArrayList<BufferedReader>();
        for (Enumeration element = zip.entries(); element.hasMoreElements(); ) {
            ZipEntry entry = (ZipEntry) element.nextElement();
            if (!entry.isDirectory()) {
                InputStream in = zip.getInputStream(entry);
                InputStreamReader inputStreamReader = new InputStreamReader(in);
                reader.add(new BufferedReader(inputStreamReader));
            }
        }

        return reader;
    }

}
package com.reltio.rocs.service;

import java.util.List;
import java.util.Properties;

import com.reltio.cst.service.ReltioAPIService;

public abstract class Export {

    protected Properties config;

    protected ReltioAPIService service;

    public List<String> exportData(String version) throws Exception {

        throw new Exception("Please implement Method 'exportData' in subclasses!!");
    }

    public Properties getConfig() {
        return config;
    }

    public void setConfig(Properties config) {
        this.config = config;
    }

    public ReltioAPIService getService() {
        return service;
    }

    public void setService(ReltioAPIService service) {
        this.service = service;
    }

}

package com.reltio.rocs.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.rocs.export.dto.RequestBody;

public class ESExport extends CassandraExport {

    private static final Logger LOG = LogManager.getLogger(ESExport.class);

    private static final Gson GSON = new Gson();

    public ESExport(Properties config, ReltioAPIService service) {
        super(config, service);
    }

    @Override
    public List<String> exportData(String version) throws Exception {


        String exportApiUrl;
        if (version.equalsIgnoreCase("v1")) {

            exportApiUrl = "https://" + config.getProperty("ENVIRONMENT_URL") + "/jobs/export/"
                    + config.getProperty("TENANT_ID") + "/" + config.getProperty("TYPE_OF_DATA", "entities")
                    + "/_scan?filter=" + config.getProperty("FILTER") + "&fileFormat="
                    + config.getProperty("FILE_FORMAT", "json") + "&async=true";

        } else {
            exportApiUrl = "https://" + config.getProperty("ENVIRONMENT_URL") + "/jobs/export/"
                    + config.getProperty("TENANT_ID") + "/" + config.getProperty("TYPE_OF_DATA", "entities")
                    + "?filter=" + config.getProperty("FILTER") + "&fileFormat="
                    + config.getProperty("FILE_FORMAT", "json");

        }
        if (config.getProperty("SELECT") != null) {
            exportApiUrl += "&select=" + config.getProperty("SELECT");
        }

        String response = "";

        try {

            LOG.debug("Posting export api call now " + exportApiUrl);
            String jsonRequestBody = prepareExportRequest();
            Map<String, String> headers = new HashMap<String, String>();
            headers.put("Content-Type", "application/json");
            response = service.post(exportApiUrl, headers, jsonRequestBody);

        } catch (ReltioAPICallFailureException re) {
            LOG.error("Error exporting  url = " + exportApiUrl + re.getErrorResponse());
            throw re;
        }

        List<String> filePaths = downLoadExportFile(response).stream().map(file -> file.getAbsolutePath()).collect(Collectors.toList());
        LOG.debug("exportData ended {}", filePaths);

        return filePaths;
    }

    private String prepareExportRequest() {

        RequestBody request = new RequestBody();
        request.setOvOnly(config.getProperty("OV_ONLY", "true"));
        return GSON.toJson(request);
    }
}

package com.reltio.rocs.service;

import static com.reltio.rocsutil.ExportUtil.decryptExportUrl;
import static com.reltio.rocsutil.ExportUtil.downloadFile;
import static com.reltio.rocsutil.ExportUtil.getTaskDetails;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.rocs.export.dto.DBExportRequest;
import com.reltio.rocs.export.dto.ExportResponse;
import com.reltio.rocs.export.dto.TaskResponse;
import com.reltio.rocs.export.exceptions.ExportException;

@Deprecated
public class CassandraExport extends Export {

    private static final Logger LOG = LogManager.getLogger(CassandraExport.class);

    private Map<String, String> fileExtentions = new HashMap<>();

    public CassandraExport(Properties config, ReltioAPIService service) {

        this.config = config;
        this.service = service;
        fileExtentions.put("ZIP", "zip");
        fileExtentions.put("GZIP", "gz");
    }

    @Override
    public List<String> exportData(String version) throws Exception {

        if (LOG.isDebugEnabled()) {
            LOG.debug("exportData started");
        }
        String exportApiUrl = "https://" + config.getProperty("ENVIRONMENT_URL") + "/jobs/export/"
                + config.getProperty("TENANT_ID") + "/" + config.getProperty("TYPE_OF_DATA", "entities")
                + "/_dbscan?filter=" + config.getProperty("FILTER") + "&fileFormat="
                + config.getProperty("FILE_FORMAT", "json") + "&async=true";

        if (config.getProperty("SELECT") != null) {
            exportApiUrl += "&select=" + config.getProperty("SELECT");
        }

        String body = prepareDBExportRequest();

        String response = "";

        try {

            LOG.debug("Posting export api call now " + exportApiUrl + "\nRequest data = " + body);
            response = service.post(exportApiUrl, body);

        } catch (ReltioAPICallFailureException re) {
            LOG.error("Error exporting  url = " + exportApiUrl + re.getErrorResponse());
            throw re;
        }

		List<File> files = downLoadExportFile(response);

		List<String> filePaths = files.stream().map(file->file.getAbsolutePath()).collect(Collectors.toList());

        if (LOG.isDebugEnabled()) {
            LOG.debug("exportData ended");
        }
        return filePaths;
    }

    private String prepareDBExportRequest() {

        DBExportRequest result = new DBExportRequest();

        result.setIncludeType(Arrays.asList("configuration/entityTypes/" + config.getProperty("ENTITY_TYPE")));
        result.setOvOnly(config.getProperty("OV_ONLY", "true"));

        return new Gson().toJson(result);
    }

    protected List<File> downLoadExportFile(String response) throws Exception {

        ExportResponse export = new Gson().fromJson(response, ExportResponse.class);

        boolean isTaskComplete = false;

        TaskResponse taskResponse = null;

        int wait = Integer.parseInt(config.getProperty("WAIT_TIME_FOR_TASK_STATUS_POLLING", "10"));

        long maxSecondsToComplete = Integer
                .parseInt(config.getProperty("MAX_TIME_WAIT_TO_COMPLETE_IN_SECONDS", "3600"));

        long exportStartTime = System.currentTimeMillis();

        String taskUrl = "https://" + config.getProperty("ENVIRONMENT_URL") + "/jobs/" + config.getProperty("TENANT_ID")
                + "/tasks/";

        while (!isTaskComplete) {

            taskResponse = getTaskDetails(export.getTaskIds(), taskUrl, service);

            if ("COMPLETED".equalsIgnoreCase(taskResponse.getStatus())) {
                Thread.sleep(
                        1000 * Integer.parseInt(config.getProperty("SLEEP_TIME_INSECONDS_AFTER_TASK_COMPLETES", "20")));

                isTaskComplete = true;
            } else if ("FAILED".equalsIgnoreCase(taskResponse.getStatus())) {
                throw new Exception("Export job is failed = " + taskResponse.getError());
            } else if ("CANCELED".equalsIgnoreCase(taskResponse.getStatus())) {
                throw new Exception("Export job is CANCELED = " + taskResponse.getError());
            } else {

                LOG.debug("Sleeping  = {} sec as task status = {}, , Current status is = ", wait, taskResponse.getStatus(), taskResponse.getCurrentState().getStatus());

                Thread.sleep(wait * 1000);

                taskResponse = getTaskDetails(export.getTaskIds(), taskUrl, service);

                if (TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - exportStartTime) > maxSecondsToComplete) {
                    LOG.error("Max wait over = " + maxSecondsToComplete + ", existing now");
                    System.exit(0);
                }

                continue;
            }

            LOG.debug("Task status Id = {}, now {}", taskResponse.getId(), taskResponse.getStatus());
        }

        LOG.debug("Export response = {}", response);

        List<File> files = new ArrayList<>();

        String decryptApiUrl = "https://" + config.getProperty("ENVIRONMENT_URL") + "/jobs/export/"
                + config.getProperty("TENANT_ID") + "/decryptUrl";

        if (taskResponse.getCurrentState().getExportUrl() == null) {

            List<String> exportUrls = taskResponse.getCurrentState().getExportUrls();

            String fileCompression = taskResponse.getParameters().getFileCompression();

            String fileDir = config.getProperty("EXPORT_FILE_PATH") == null ? "exportedFiles"
                    : config.getProperty("EXPORT_FILE_PATH");

            exportUrls.forEach(exportUrl -> {

                try {

                    String decryptedUrl = decryptExportUrl(decryptApiUrl, service,
                            exportUrl.split("downloadExportUrl=")[1]);

                    files.add(downloadFile(decryptedUrl,
                            "/part_" + System.currentTimeMillis() + "." + fileExtentions.get(fileCompression),
                            fileDir));

                } catch (Exception e) {
                    LOG.error("Failed tp download the files exportUrl = " + exportUrl, e);
                    throw new ExportException(e);
                }
            });

            return files;

        } else {

            String exportUrl = taskResponse.getCurrentState().getExportUrl().split("downloadExportUrl=")[1];

            String decryptedUrl = decryptExportUrl(decryptApiUrl, service, exportUrl);

            File singleFile = downloadFile(decryptedUrl,
                    config.getProperty("EXPORT_FILE") == null ? System.currentTimeMillis() + "." + fileExtentions.get(taskResponse.getParameters().getFileCompression())
                            : config.getProperty("EXPORT_FILE"));
            files.add(singleFile);

            return files;
        }
    }
}
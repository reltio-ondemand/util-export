/**
 * 
 */
package com.reltio.rocs.export.exceptions;

/**
 * @author spatil
 *
 */
public class ExportException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ExportException() {
	}

	public ExportException(Throwable e) {
		super(e);
	}

	public ExportException(String message, Throwable e) {
		super(message, e);
	}
}


package com.reltio.rocs.export.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GlobalStatus {

	@SerializedName("lastFiveMinReadThroughput")
	@Expose
	private Integer lastFiveMinReadThroughput;
	@SerializedName("lastFiveMinThroughput")
	@Expose
	private Integer lastFiveMinThroughput;
	@SerializedName("entitiesTotal")
	@Expose
	private Integer entitiesTotal;
	@SerializedName("entitiesNumber")
	@Expose
	private Integer entitiesNumber;
	@SerializedName("overallThroughput")
	@Expose
	private Integer overallThroughput;
	@SerializedName("overallReadThroughput")
	@Expose
	private Integer overallReadThroughput;

	public Integer getLastFiveMinReadThroughput() {
		return lastFiveMinReadThroughput;
	}

	public void setLastFiveMinReadThroughput(Integer lastFiveMinReadThroughput) {
		this.lastFiveMinReadThroughput = lastFiveMinReadThroughput;
	}

	public Integer getLastFiveMinThroughput() {
		return lastFiveMinThroughput;
	}

	public void setLastFiveMinThroughput(Integer lastFiveMinThroughput) {
		this.lastFiveMinThroughput = lastFiveMinThroughput;
	}

	public Integer getEntitiesTotal() {
		return entitiesTotal;
	}

	public void setEntitiesTotal(Integer entitiesTotal) {
		this.entitiesTotal = entitiesTotal;
	}

	public Integer getEntitiesNumber() {
		return entitiesNumber;
	}

	public void setEntitiesNumber(Integer entitiesNumber) {
		this.entitiesNumber = entitiesNumber;
	}

	public Integer getOverallThroughput() {
		return overallThroughput;
	}

	public void setOverallThroughput(Integer overallThroughput) {
		this.overallThroughput = overallThroughput;
	}

	public Integer getOverallReadThroughput() {
		return overallReadThroughput;
	}

	public void setOverallReadThroughput(Integer overallReadThroughput) {
		this.overallReadThroughput = overallReadThroughput;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GlobalStatus [lastFiveMinReadThroughput=").append(lastFiveMinReadThroughput)
				.append(", lastFiveMinThroughput=").append(lastFiveMinThroughput).append(", entitiesTotal=")
				.append(entitiesTotal).append(", entitiesNumber=").append(entitiesNumber).append(", overallThroughput=")
				.append(overallThroughput).append(", overallReadThroughput=").append(overallReadThroughput).append("]");
		return builder.toString();
	}

}

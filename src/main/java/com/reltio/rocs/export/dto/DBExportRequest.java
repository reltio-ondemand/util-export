package com.reltio.rocs.export.dto;

import java.util.List;

public class DBExportRequest {

	private List<String> includeType;
	private String ovOnly;
	public List<String> getIncludeType() {
		return includeType;
	}
	public void setIncludeType(List<String> includeType) {
		this.includeType = includeType;
	}
	public String getOvOnly() {
		return ovOnly;
	}
	public void setOvOnly(String ovOnly) {
		this.ovOnly = ovOnly;
	}
	@Override
	public String toString() {
		return "DBExportRequest [includeType=" + includeType + ", ovOnly=" + ovOnly + "]";
	}



}


package com.reltio.rocs.export.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Parameters {

	@SerializedName("taskId")
	@Expose
	private String taskId;

	@SerializedName("fileCompression")
	@Expose
	private String fileCompression;

	@SerializedName("useEsHeadersCollectionExport")
	@Expose
	private String useEsHeadersCollectionExport;
	@SerializedName("fileFormat")
	@Expose
	private String fileFormat;
	@SerializedName("userRoles")
	@Expose
	private String userRoles;
	@SerializedName("exportName")
	@Expose
	private String exportName;
	@SerializedName("dateFormat")
	@Expose
	private String dateFormat;
	@SerializedName("specifiedMailAddress")
	@Expose
	private String specifiedMailAddress;
	@SerializedName("tenantId")
	@Expose
	private String tenantId;
	@SerializedName("exploded")
	@Expose
	private String exploded;
	@SerializedName("username")
	@Expose
	private String username;
	@SerializedName("queryParams")
	@Expose
	private String queryParams;
	@SerializedName("encryptUrl")
	@Expose
	private String encryptUrl;
	@SerializedName("exportPath")
	@Expose
	private String exportPath;
	@SerializedName("mailAddress")
	@Expose
	private String mailAddress;
	@SerializedName("ovOnly")
	@Expose
	private String ovOnly;

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getUseEsHeadersCollectionExport() {
		return useEsHeadersCollectionExport;
	}

	public void setUseEsHeadersCollectionExport(String useEsHeadersCollectionExport) {
		this.useEsHeadersCollectionExport = useEsHeadersCollectionExport;
	}

	public String getFileFormat() {
		return fileFormat;
	}

	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}

	public String getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(String userRoles) {
		this.userRoles = userRoles;
	}

	public String getExportName() {
		return exportName;
	}

	public void setExportName(String exportName) {
		this.exportName = exportName;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getSpecifiedMailAddress() {
		return specifiedMailAddress;
	}

	public void setSpecifiedMailAddress(String specifiedMailAddress) {
		this.specifiedMailAddress = specifiedMailAddress;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getExploded() {
		return exploded;
	}

	public void setExploded(String exploded) {
		this.exploded = exploded;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getQueryParams() {
		return queryParams;
	}

	public void setQueryParams(String queryParams) {
		this.queryParams = queryParams;
	}

	public String getEncryptUrl() {
		return encryptUrl;
	}

	public void setEncryptUrl(String encryptUrl) {
		this.encryptUrl = encryptUrl;
	}

	public String getExportPath() {
		return exportPath;
	}

	public void setExportPath(String exportPath) {
		this.exportPath = exportPath;
	}

	public String getMailAddress() {
		return mailAddress;
	}

	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}

	public String getOvOnly() {
		return ovOnly;
	}

	public void setOvOnly(String ovOnly) {
		this.ovOnly = ovOnly;
	}

	public String getFileCompression() {
		return fileCompression;
	}

	public void setFileCompression(String fileCompression) {
		this.fileCompression = fileCompression;
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("Parameters{");
		sb.append("taskId='").append(taskId).append('\'');
		sb.append(", fileCompression='").append(fileCompression).append('\'');
		sb.append(", useEsHeadersCollectionExport='").append(useEsHeadersCollectionExport).append('\'');
		sb.append(", fileFormat='").append(fileFormat).append('\'');
		sb.append(", userRoles='").append(userRoles).append('\'');
		sb.append(", exportName='").append(exportName).append('\'');
		sb.append(", dateFormat='").append(dateFormat).append('\'');
		sb.append(", specifiedMailAddress='").append(specifiedMailAddress).append('\'');
		sb.append(", tenantId='").append(tenantId).append('\'');
		sb.append(", exploded='").append(exploded).append('\'');
		sb.append(", username='").append(username).append('\'');
		sb.append(", queryParams='").append(queryParams).append('\'');
		sb.append(", encryptUrl='").append(encryptUrl).append('\'');
		sb.append(", exportPath='").append(exportPath).append('\'');
		sb.append(", mailAddress='").append(mailAddress).append('\'');
		sb.append(", ovOnly='").append(ovOnly).append('\'');
		sb.append('}');
		return sb.toString();
	}
}

package com.reltio.rocs.export.dto;

public class RequestBody {
	private String ovOnly;

	public String getOvOnly() {
		return ovOnly;
	}

	public void setOvOnly(String ovOnly) {
		this.ovOnly = ovOnly;
	}

	@Override
	public String toString() {
		return "RequestBody [ovOnly=" + ovOnly + "]";
	}

}

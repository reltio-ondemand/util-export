
package com.reltio.rocs.reltioexporter.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TaskResponse {

	@SerializedName("id")
	@Expose
	private String id;
	@SerializedName("createdTime")
	@Expose
	private Long createdTime;
	@SerializedName("createdBy")
	@Expose
	private String createdBy;
	@SerializedName("updatedTime")
	@Expose
	private Long updatedTime;
	@SerializedName("updatedBy")
	@Expose
	private String updatedBy;
	@SerializedName("type")
	@Expose
	private String type;
	@SerializedName("status")
	@Expose
	private String status;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("createdOnHost")
	@Expose
	private String createdOnHost;
	@SerializedName("executedOnHost")
	@Expose
	private String executedOnHost;
	@SerializedName("parallelExecution")
	@Expose
	private Boolean parallelExecution;
	@SerializedName("nodesGroup")
	@Expose
	private String nodesGroup;
	@SerializedName("startTime")
	@Expose
	private String startTime;
	@SerializedName("endTime")
	@Expose
	private String endTime;
	@SerializedName("parameters")
	@Expose
	private Parameters parameters;
	@SerializedName("currentState")
	@Expose
	private CurrentState currentState;
	@SerializedName("throughput")
	@Expose
	private Double throughput;
	@SerializedName("duration")
	@Expose
	private String duration;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Long createdTime) {
		this.createdTime = createdTime;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Long getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Long updatedTime) {
		this.updatedTime = updatedTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreatedOnHost() {
		return createdOnHost;
	}

	public void setCreatedOnHost(String createdOnHost) {
		this.createdOnHost = createdOnHost;
	}

	public String getExecutedOnHost() {
		return executedOnHost;
	}

	public void setExecutedOnHost(String executedOnHost) {
		this.executedOnHost = executedOnHost;
	}

	public Boolean getParallelExecution() {
		return parallelExecution;
	}

	public void setParallelExecution(Boolean parallelExecution) {
		this.parallelExecution = parallelExecution;
	}

	public String getNodesGroup() {
		return nodesGroup;
	}

	public void setNodesGroup(String nodesGroup) {
		this.nodesGroup = nodesGroup;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Parameters getParameters() {
		return parameters;
	}

	public void setParameters(Parameters parameters) {
		this.parameters = parameters;
	}

	public CurrentState getCurrentState() {
		return currentState;
	}

	public void setCurrentState(CurrentState currentState) {
		this.currentState = currentState;
	}

	public Double getThroughput() {
		return throughput;
	}

	public void setThroughput(Double throughput) {
		this.throughput = throughput;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TaskResponse [id=").append(id).append(", createdTime=").append(createdTime)
				.append(", createdBy=").append(createdBy).append(", updatedTime=").append(updatedTime)
				.append(", updatedBy=").append(updatedBy).append(", type=").append(type).append(", status=")
				.append(status).append(", name=").append(name).append(", createdOnHost=").append(createdOnHost)
				.append(", executedOnHost=").append(executedOnHost).append(", parallelExecution=")
				.append(parallelExecution).append(", nodesGroup=").append(nodesGroup).append(", startTime=")
				.append(startTime).append(", endTime=").append(endTime).append(", parameters=").append(parameters)
				.append(", currentState=").append(currentState).append(", throughput=").append(throughput)
				.append(", duration=").append(duration).append("]");
		return builder.toString();
	}

}

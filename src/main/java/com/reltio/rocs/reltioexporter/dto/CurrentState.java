
package com.reltio.rocs.reltioexporter.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrentState {

	@SerializedName("globalStatus")
	@Expose
	private GlobalStatus globalStatus;
	@SerializedName("lastHourThroughput")
	@Expose
	private Double lastHourThroughput;
	@SerializedName("statusId")
	@Expose
	private Integer statusId;
	@SerializedName("numberOfProcessedObjects")
	@Expose
	private Integer numberOfProcessedObjects;
	@SerializedName("numberOfObjects")
	@Expose
	private Integer numberOfObjects;
	@SerializedName("exportUrl")
	@Expose
	private String exportUrl;
	@SerializedName("numberOfFailedToPublishEvents")
	@Expose
	private Integer numberOfFailedToPublishEvents;
	@SerializedName("status")
	@Expose
	private String status;

	public GlobalStatus getGlobalStatus() {
		return globalStatus;
	}

	public void setGlobalStatus(GlobalStatus globalStatus) {
		this.globalStatus = globalStatus;
	}

	public Double getLastHourThroughput() {
		return lastHourThroughput;
	}

	public void setLastHourThroughput(Double lastHourThroughput) {
		this.lastHourThroughput = lastHourThroughput;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Integer getNumberOfProcessedObjects() {
		return numberOfProcessedObjects;
	}

	public void setNumberOfProcessedObjects(Integer numberOfProcessedObjects) {
		this.numberOfProcessedObjects = numberOfProcessedObjects;
	}

	public Integer getNumberOfObjects() {
		return numberOfObjects;
	}

	public void setNumberOfObjects(Integer numberOfObjects) {
		this.numberOfObjects = numberOfObjects;
	}

	public String getExportUrl() {
		return exportUrl;
	}

	public void setExportUrl(String exportUrl) {
		this.exportUrl = exportUrl;
	}

	public Integer getNumberOfFailedToPublishEvents() {
		return numberOfFailedToPublishEvents;
	}

	public void setNumberOfFailedToPublishEvents(Integer numberOfFailedToPublishEvents) {
		this.numberOfFailedToPublishEvents = numberOfFailedToPublishEvents;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CurrentState [globalStatus=").append(globalStatus).append(", lastHourThroughput=")
				.append(lastHourThroughput).append(", statusId=").append(statusId).append(", numberOfProcessedObjects=")
				.append(numberOfProcessedObjects).append(", numberOfObjects=").append(numberOfObjects)
				.append(", exportUrl=").append(exportUrl).append(", numberOfFailedToPublishEvents=")
				.append(numberOfFailedToPublishEvents).append(", status=").append(status).append("]");
		return builder.toString();
	}

}

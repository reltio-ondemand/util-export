package com.reltio.rocs.reltioexporter.builder;

import java.util.Properties;

import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.util.Util;
import com.reltio.rocs.service.ESExport;
import com.reltio.rocs.service.Export;

public abstract class ExportServiceBuilder {

    private ExportServiceBuilder() {
    }

    public static Export build(Properties config, ReltioAPIService service) throws Exception {

        Export result = null;

        String type = config.getProperty("EXPORT_TYPE");
        String version = config.getProperty("VERSION", "v2");

        if (Util.isEmpty(type)) {
            throw new Exception("EXPORT_TYPE is not exist in configuration!!");
        }

        if (version.equalsIgnoreCase("v2")) {
            result = new ESExport(config, service);
            return result;
        }
        
        switch (type) {

            case "esexport": {
                result = new ESExport(config, service);
                break;
            }

            case "cassandraexport": {
            	 throw new Exception("cassandraexport is not supported please use version v2");
            }

            default: {
                throw new Exception("No such export supported!!" + type);
            }

        }

        return result;
    }
}
package com.reltio.test.reltioexporter;

import java.io.BufferedReader;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import com.reltio.rocs.main.Exporter;

public class TestClassReader {
    @Test
    @Ignore
    public void readZipFile() throws Exception {
        Exporter main = new Exporter();
        List<BufferedReader> readers = main.readZipFile("export.zip");
        for (BufferedReader reader : readers) {
            String line = null;
            int size = 0;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
                size++;
            }
            System.out.println(size);
        }
    }
}
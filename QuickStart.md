# Quick Start 

##Building
* Deployment instructions

    The main method of the application is at the following path:
    com.reltio.rocs.main.Exporter    

    please run “mvn clean” at the root where the pom.xml file is located and 
    maven will install the library automatically. Then you could run “mvn package” to build the jar file.



##Parameters File Example

```
#Common Properties
ENVIRONMENT_URL=pilot.reltio.com
TENANT_ID=ugOdKcVPJghN0V7
AUTH_URL=https\://auth.reltio.com/oauth/token

USERNAME=vignesh.chandran@reltio.com
PASSWORD=****************

#or

CLIENT_CREDENTIALS=*******************

HTTP_PROXY_HOST=
HTTP_PROXY_PORT=

#Tool Specific Properties

EXPORT_TYPE=cassandraexport
ENTITY_TYPE=Employee

FILE_FORMAT=json
TYPE_OF_DATA=entities
EXPORT_FILE_FORMAT=json
FILTER=equals(type,'configuration/entityTypes/Employee')

EXPORT_FILE_PATH=export
EXPORT_FILE=data.zip

MAX_TIME_WAIT_TO_COMPLETE_IN_SECONDS=200000
SLEEP_TIME_INSECONDS_AFTER_TASK_COMPLETES=20
WAIT_TIME_FOR_TASK_STATUS_POLLING=10


```
##Executing

Command to start the utility.
```
#Windows
java -jar util-export-{{version}}-jar-with-dependencies.jar “configuration.properties” > $logfilepath$'


#Unix
java -jar util-export-{{version}}-jar-with-dependencies.jar config.properties > $logfilepath$

```